import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[appPvChartDirective]'
})
export class PvChartDirective {
  constructor(public viewContainerRef: ViewContainerRef) {}
}
