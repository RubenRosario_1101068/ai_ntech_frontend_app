import { LinhaComponent } from './ecra/linha/linha.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { EcraComponent } from './ecra/ecra.component';

const routes: Routes = [
  {
    path: 'ecra/:id',
    component: EcraComponent
  },
  {
    path: '',
    component: LinhaComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
