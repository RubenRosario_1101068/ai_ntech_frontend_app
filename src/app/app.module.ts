import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EcraComponent } from './ecra/ecra.component';
import { LinhaComponent } from './ecra/linha/linha.component';
import { LoteComponent } from './ecra/lote/lote.component';
import { TotalComponent } from './ecra/total/total.component';
import { ApexChartComponent } from './apex-chart/apex-chart.component';
import { HttpClientModule } from '@angular/common/http';
import {
  MatTableModule,
  MatCardModule,
  MatButtonModule
} from '@angular/material';
import { PvChartDirective } from './_directives/pv-chart.directive';

@NgModule({
  declarations: [
    AppComponent,
    EcraComponent,
    LinhaComponent,
    LoteComponent,
    TotalComponent,
    ApexChartComponent,
    PvChartDirective
  ],
  exports: [MatTableModule],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatTableModule,
    HttpClientModule,
    MatCardModule,
    MatButtonModule
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents: [ApexChartComponent]
})
export class AppModule {}
