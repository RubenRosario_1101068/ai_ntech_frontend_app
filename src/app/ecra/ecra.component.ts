import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ecra',
  templateUrl: './ecra.component.html',
  styleUrls: ['./ecra.component.css']
})
export class EcraComponent implements OnInit {
  id: string;
  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.paramMap.subscribe(params => {
      this.id = params.get('id');
    });
    // ler da bd todos os parâmetros
    // verificar qual é o tipo de ecrã (global, linha ou lote)

  }
}
