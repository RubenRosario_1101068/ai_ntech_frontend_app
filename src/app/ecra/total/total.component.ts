import { Component, OnInit, Input } from '@angular/core';
import { Producao } from 'src/app/_interfaces/producao.interface';

export interface Tempo {
  descritivo: string;
  tempo: number;
  percentagem: number;
}

export interface PeakValue {
  descritivo: string;
  valor: number;
}

@Component({
  selector: 'app-total',
  templateUrl: './total.component.html',
  styleUrls: ['./total.component.css']
})
export class TotalComponent implements OnInit {
  @Input() periodo: number;
  @Input() turno: string;
  optionsProducao: any;
  optionsTempos: any;
  optionsDistribuicao: any;
  aceites: number;
  rejeitadas1: number;
  rejeitadas2: number;
  producaoTotal: number;
  tempoOperacao: number;
  tempoParagem: number;
  tempoTotal: number;

  colunasProducao: string[] = ['classificacao', 'quantidade', 'percentagem'];
  colunasTempos: string[] = ['descritivo', 'tempo', 'percentagem'];

  producao: Producao[];
  tempos: Tempo[];
  peakValues: PeakValue[];
  esperarCarregar = true;

  constructor() {}

  ngOnInit() {
    // ler dados da bd
    this.aceites = 2349;
    this.rejeitadas1 = 55;
    this.rejeitadas2 = 13;
    this.producaoTotal = this.aceites + this.rejeitadas1 + this.rejeitadas2;
    this.tempoOperacao = 1000;
    this.tempoParagem = 250;
    this.tempoTotal = this.tempoOperacao + this.tempoParagem;
    this.peakValues = [
      {
        descritivo: '0',
        valor: 100
      },
      {
        descritivo: '1',
        valor: 200
      },
      {
        descritivo: '2',
        valor: 300
      },
      {
        descritivo: '3',
        valor: 400
      },
      {
        descritivo: '4',
        valor: 500
      },
      {
        descritivo: '5',
        valor: 600
      },
      {
        descritivo: '6',
        valor: 700
      },
      {
        descritivo: '7',
        valor: 800
      },
      {
        descritivo: '8',
        valor: 900
      },
      {
        descritivo: '9',
        valor: 1000
      },
      {
        descritivo: '10',
        valor: 1100
      },
      {
        descritivo: '11',
        valor: 1200
      },
      {
        descritivo: '12',
        valor: 1300
      },
      {
        descritivo: '13',
        valor: 1200
      },
      {
        descritivo: '14',
        valor: 1100
      },
      {
        descritivo: '15',
        valor: 1000
      },
      {
        descritivo: '16',
        valor: 900
      },
      {
        descritivo: '17',
        valor: 800
      },
      {
        descritivo: '18',
        valor: 700
      },
      {
        descritivo: '19',
        valor: 600
      },
      {
        descritivo: '20',
        valor: 500
      },
      {
        descritivo: '21',
        valor: 400
      },
      {
        descritivo: '22',
        valor: 300
      },
      {
        descritivo: '24',
        valor: 200
      },
      {
        descritivo: '25',
        valor: 100
      },
      {
        descritivo: '>=25',
        valor: 50
      }
    ];

    this.producao = [
      {
        classificacao: 'Aceites',
        quantidade: this.aceites,
        percentagem: Math.round((this.aceites / this.producaoTotal) * 1000) / 10
      },
      {
        classificacao: 'Rejeitadas 1',
        quantidade: this.rejeitadas1,
        percentagem:
          Math.round((this.rejeitadas1 / this.producaoTotal) * 1000) / 10
      },
      {
        classificacao: 'Rejeitadas 2',
        quantidade: this.rejeitadas2,
        percentagem:
          Math.round((this.rejeitadas2 / this.producaoTotal) * 1000) / 10
      }
    ];

    this.tempos = [
      {
        descritivo: 'Operação',
        tempo: this.tempoOperacao,
        percentagem:
          Math.round((this.tempoOperacao / this.tempoTotal) * 1000) / 10
      },
      {
        descritivo: 'Paragem',
        tempo: this.tempoParagem,
        percentagem:
          Math.round((this.tempoParagem / this.tempoTotal) * 1000) / 10
      }
    ];

    let _aux = true;
    const _descritivos = [];
    this.peakValues.forEach(element => {
      if (_aux) {
        _descritivos.push(element.descritivo);
      } else {
        _descritivos.push('');
      }
      _aux = !_aux;
    });
    const _valores = [];
    this.peakValues.forEach(element => {
      _valores.push(element.valor);
    });

    this.optionsProducao = {
      chart: {
        type: 'pie'
      },
      labels: ['Aceites', 'Rejeitadas 1', 'Rejeitadas 2'],
      series: [this.aceites, this.rejeitadas1, this.rejeitadas2],
      legend: {
        position: 'bottom',
        verticalAlign: 'top'
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom'
            }
          }
        }
      ]
    };

    this.optionsTempos = {
      chart: {
        type: 'pie'
      },
      labels: ['Operação', 'Paragem'],
      series: [this.tempoOperacao, this.tempoParagem],
      legend: {
        position: 'bottom',
        verticalAlign: 'top'
      },
      responsive: [
        {
          breakpoint: 480,
          options: {
            legend: {
              position: 'bottom'
            }
          }
        }
      ],
      theme: {
        palette: 'palette6' // upto palette10
      }
    };

    this.optionsDistribuicao = {
      chart: {
        height: 350,
        type: 'bar'
      },
      plotOptions: {
        bar: {
          horizontal: false,
          endingShape: 'rounded',
          columnWidth: '55%'
        }
      },
      dataLabels: {
        enabled: false
      },
      stroke: {
        show: true,
        width: 2,
        colors: ['transparent']
      },
      series: [
        {
          data: _valores
        }
      ],
      xaxis: {
        categories: _descritivos
      },
      yaxis: {
        title: {
          text: 'valor'
        }
      },
      fill: {
        opacity: 1
      }
    };
  }
}
