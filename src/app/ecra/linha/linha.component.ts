import { ApexChartComponent } from './../../apex-chart/apex-chart.component';
import {
  Component,
  OnInit,
  ViewChild,
  OnDestroy,
  ComponentFactoryResolver,
  ComponentFactory,
  ComponentRef
} from '@angular/core';
import { SubscriptionLike, interval } from 'rxjs';
import * as moment from 'moment';
import { RegistoLinha } from './../../_interfaces/registo-linha.interface';
import { GetRegistosLinhaService } from './../../_services/get-registos-linha.service';
import { MatTableDataSource } from '@angular/material';

import * as math from 'mathjs';
import { NgModuleCompileResult } from '@angular/compiler/src/ng_module_compiler';
import { PvChartDirective } from 'src/app/_directives/pv-chart.directive';
import { DataSource } from '@angular/cdk/table';

interface ProdutoPorModulo {
  modulo: string;
  totalAceita: number;
  totalRejeitado1: number;
  totalRejeitado2: number;
}

interface DadoTabela {
  produto: string;
  total: number;
  percentagem: number;
}

interface DadoTabelaParagem {
  idModulo: string;
  totalParagem: number;
  totalOperacao: number;
  ultimaParagem: string;
  ultimaRetoma: string;
}

interface Modulo {
  idModulo: string;
  periodosOperacao: string[]; // cada par de valores corresponde ao inicio e fim de um operação

  // array de o peak value por intervalor
  // [0-1[, [1-2[, [2-3[, [3-4[, [4- inf[
  peakValues: number[];
}

// variaveis globais
const hora = '2019-02-01 00:19:00'; // data/hora de fim de pesquisa
const linha = '10';
const periodoPesquisa = 1; // periodo, em horas, a mostrar no grafico
const intervaloAtualizacao = 5; // intervalo, em segundos, para refresh do gráfico
const offset = 5; // deslocacao do grafico em minutos
const formatoData = 'YYYY-MM-DD HH:mm:ss';

@Component({
  selector: 'app-linha',
  templateUrl: './linha.component.html',
  styleUrls: ['./linha.component.css']
})
export class LinhaComponent implements OnInit, OnDestroy {
  registos: RegistoLinha[]; // registo raw vindo do webservice dentro do periodo indicado
  produtosPorModulo: ProdutoPorModulo[]; // acenteis e rejeitados por cada módulo
  horaInicial: string;
  horaFinal: string;
  idLinha: string;
  modulos: Modulo[]; // array com os módulos extraido do pedido
  xaxis: string[]; // array com os labels para o eixo dos X no gŕafico da producao
  xaxisDN = ['[0-1[', '[1-2[', '[2-3[', '[3-4[', '[4-∞[']; // array com os labels para o eixo dos X no gŕafico da distribuicao normal
  xaxisP = []; // array com labels para o gráfico paragem
  chartSerie = []; // serie do gráfico de produção
  chartSerieP = []; // sérir do gráfico das paragens
  totalAceite = 0;
  totalRejeitado1 = 0;
  totalRejeitado2 = 0;
  totalProcessado = 0;
  colunasTabelaProducao = ['produto', 'total', 'percentagem'];
  colunasTabelaParagem = ['tempos'];
  optionsProducao: any; // parametros para o gráfico da produção
  optionsParagem: any; // parâmentros para o  gráfico das paragens
  optionsPeakValue: any; // parametros para o gráfico da distribuiçao normal
  aceitesGrafico: any; // serie aceites no grafico
  rejeitados1Grafico: any; //  serie rejeitados 1 no gráfico
  rejeitados2Grafico: any; // serie rejeitados 2 no gráfico
  dadosTabela: DadoTabela[]; // tabela da producao
  dadosTabelaParagem: DadoTabelaParagem[]; // tabela das paragens
  dataSource;
  esperarCarregar = true;
  subscriptions: SubscriptionLike[] = new Array<SubscriptionLike>();
  isfullscreen = false;
  flagSemDados = false; // indica se o pedido anterior recebeu dados ou nao

  @ViewChild('chartProducao')
  chartProducao;

  @ViewChild('chartParagem')
  chartParagem;

  @ViewChild('chartPeakValue0')
  chartPV0;

  @ViewChild('chartPeakValue1')
  chartPV1;

  @ViewChild('chartPeakValue2')
  chartPV2;

  @ViewChild('chartPeakValue3')
  chartPV3;

  @ViewChild('chartPeakValue4')
  chartPV4;

  @ViewChild('chartPeakValue5')
  chartPV5;

  constructor(
    private getRegistosLinhaService: GetRegistosLinhaService,
    private componentFactoryResolver: ComponentFactoryResolver
  ) {
    this.horaFinal = moment(hora, formatoData).format(formatoData);
    this.horaInicial = moment(hora, formatoData)
      .subtract(offset, 'minutes')
      .format(formatoData);
    this.idLinha = linha;
    this.registos = new Array<RegistoLinha>();
    this.produtosPorModulo = new Array<ProdutoPorModulo>();
    this.modulos = new Array<Modulo>();
    this.xaxis = new Array<string>();
    this.dadosTabela = new Array<DadoTabela>();
    this.dadosTabelaParagem = new Array<DadoTabelaParagem>();
  }

  ngOnInit() {
    this.getRegistosLinhaService
      .getRegistosLinha(this.idLinha, this.horaInicial, this.horaFinal)
      .subscribe(d => {
        if (d.response === 'ok') {
          const data = d.payload.result as RegistoLinha[];
          this.registos = data;
          // atualizar toda a informação
          this.atualizarQuadro(data);
        } else {
          alert('erro na ligação');
        }
      });
    this.updateCharts();
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => {
      s.unsubscribe();
    });
  }

  /**
   * retorna a altura que os gráficos de produraçao e paragem
   *  devem ter com base no tamanho do ecrã
   * @param tipo "producao" ou "peak value"
   */
  alturaGrafico(tipo: string) {
    switch (tipo) {
      case 'producao':
        if (screen.width > 1400 && screen.height > 800) {
          return 450;
        } else {
          return 250;
        }
      case 'peak value':
        if (screen.width > 1400 && screen.height > 800) {
          return 450;
        } else {
          return 250;
        }
    }
  }

  updateCharts() {
    this.subscriptions.forEach(s => s.unsubscribe());
    this.subscriptions.push(
      // fazer pedido ao servidor de x em x minutos segundo o intervaloAtualizacao
      interval(intervaloAtualizacao * 1000).subscribe(() => {
        this.getRegistosLinhaService
          .getRegistosLinha(this.idLinha, this.horaInicial, this.horaFinal)
          .subscribe(d => {
            if (d.response === 'ok') {
              const data = d.payload.result as RegistoLinha[];
              this.registos = this.registos.concat(data);

              // atualizar toda a informação
              this.atualizarQuadro(data);

              this.chartProducao.updateDataSeries(this.chartSerie);
              this.chartParagem.updateDataSeries(this.chartSerieP);

              this.chartPV0.updateDataSeries(this.optionsPeakValue[0].series);
              this.chartPV1.updateDataSeries(this.optionsPeakValue[1].series);
              this.chartPV2.updateDataSeries(this.optionsPeakValue[2].series);
              this.chartPV3.updateDataSeries(this.optionsPeakValue[3].series);
              this.chartPV4.updateDataSeries(this.optionsPeakValue[4].series);
              this.chartPV5.updateDataSeries(this.optionsPeakValue[5].series);
            } else {
              alert('erro na ligação');
            }
          });
      })
    );
  }

  openFullscreen() {
    // abrir fullscreen
    const docElmWithBrowsersFullScreenFunctions = document.documentElement as HTMLElement & {
      mozRequestFullScreen(): Promise<void>;
      webkitRequestFullscreen(): Promise<void>;
      msRequestFullscreen(): Promise<void>;
    };

    if (docElmWithBrowsersFullScreenFunctions.requestFullscreen) {
      docElmWithBrowsersFullScreenFunctions.requestFullscreen();
    } else if (docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen) {
      /* Firefox */
      docElmWithBrowsersFullScreenFunctions.mozRequestFullScreen();
    } else if (docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen) {
      /* Chrome, Safari e Opera */
      docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen();
    } else if (docElmWithBrowsersFullScreenFunctions.msRequestFullscreen) {
      /* IE/Edge */
      docElmWithBrowsersFullScreenFunctions.msRequestFullscreen();
    }
    this.isfullscreen = true;

    // ajustar o tamanho dos gráficos
    if (screen.height > 800) {
      this.chartProducao.updateOptions({ chart: { height: 450 } }, false, true);
      this.chartParagem.updateOptions({ chart: { height: 450 } }, false, true);
      this.chartPV0.updateOptions({ chart: { height: 380 } }, false, true);
      this.chartPV1.updateOptions({ chart: { height: 380 } }, false, true);
      this.chartPV2.updateOptions({ chart: { height: 380 } }, false, true);
      this.chartPV3.updateOptions({ chart: { height: 380 } }, false, true);
      this.chartPV4.updateOptions({ chart: { height: 380 } }, false, true);
      this.chartPV5.updateOptions({ chart: { height: 380 } }, false, true);
    } else {
      this.chartProducao.updateOptions({ chart: { height: 300 } }, false, true);
      this.chartParagem.updateOptions({ chart: { height: 300 } }, false, true);
      this.chartPV0.updateOptions({ chart: { height: 220 } }, false, true);
      this.chartPV1.updateOptions({ chart: { height: 220 } }, false, true);
      this.chartPV2.updateOptions({ chart: { height: 220 } }, false, true);
      this.chartPV3.updateOptions({ chart: { height: 220 } }, false, true);
      this.chartPV4.updateOptions({ chart: { height: 220 } }, false, true);
      this.chartPV5.updateOptions({ chart: { height: 220 } }, false, true);
    }
  }

  closeFullscreen() {
    // sair do fullscreen
    const docWithBrowsersExitFunctions = document as Document & {
      mozCancelFullScreen(): Promise<void>;
      webkitExitFullscreen(): Promise<void>;
      msExitFullscreen(): Promise<void>;
    };
    if (docWithBrowsersExitFunctions.exitFullscreen) {
      docWithBrowsersExitFunctions.exitFullscreen();
    } else if (docWithBrowsersExitFunctions.mozCancelFullScreen) {
      /* Firefox */
      docWithBrowsersExitFunctions.mozCancelFullScreen();
    } else if (docWithBrowsersExitFunctions.webkitExitFullscreen) {
      /* Chrome, Safari and Opera */
      docWithBrowsersExitFunctions.webkitExitFullscreen();
    } else if (docWithBrowsersExitFunctions.msExitFullscreen) {
      /* IE/Edge */
      docWithBrowsersExitFunctions.msExitFullscreen();
    }
    this.isfullscreen = false;
    // ajustar o tamanho dos gráficos
    if (screen.height > 800) {
      this.chartProducao.updateOptions({ chart: { height: 400 } }, false, true);
      this.chartParagem.updateOptions({ chart: { height: 400 } }, false, true);
      this.chartPV0.updateOptions({ chart: { height: 330 } }, false, true);
      this.chartPV1.updateOptions({ chart: { height: 330 } }, false, true);
      this.chartPV2.updateOptions({ chart: { height: 330 } }, false, true);
      this.chartPV3.updateOptions({ chart: { height: 330 } }, false, true);
      this.chartPV4.updateOptions({ chart: { height: 330 } }, false, true);
      this.chartPV5.updateOptions({ chart: { height: 330 } }, false, true);
    } else {
      this.chartProducao.updateOptions({ chart: { height: 250 } }, false, true);
      this.chartParagem.updateOptions({ chart: { height: 250 } }, false, true);
      this.chartPV0.updateOptions({ chart: { height: 160 } }, false, true);
      this.chartPV1.updateOptions({ chart: { height: 160 } }, false, true);
      this.chartPV2.updateOptions({ chart: { height: 160 } }, false, true);
      this.chartPV3.updateOptions({ chart: { height: 160 } }, false, true);
      this.chartPV4.updateOptions({ chart: { height: 160 } }, false, true);
      this.chartPV5.updateOptions({ chart: { height: 160 } }, false, true);
    }
  }

  atualizarQuadro(data) {
    // extrair os modulos
    this.extrairModulos(data);

    // Definir as labels do eixo tod XX no gráfico de produção
    this.xaxis = this.definirXaxis();

    // calcular tempo de operação de cada modulo
    this.calculaTempoOpModulo(data);

    // calcular totais aceites e rejeitados global
    this.calcularTotais(data);

    // calcular o total de A/R1/R2 por cada módulo por cada iteração
    this.calcularResultadosPorModulo(data);

    // construir os arrays aceites e rejeitados
    this.chartSerie = this.novoChartSeries();

    // construir serie para o gráfico das paragens
    this.chartSerieP = this.novoChartSeriesParagem();

    // definir parâmetros do gráfico
    this.definirParametros();

    // definir parâmetros do gráfico das paragens
    this.definirParametrosGraficoParagem();

    // definir parametros para o gráfico do peak value
    this.peakValue(data);

    this.esperarCarregar = false;

    // atualizar hora inicia e final para o proximo pedido
    this.horaInicial = moment(this.horaInicial, formatoData)
      .add(offset + 0.016666667, 'minutes')
      .format(formatoData);
    this.horaFinal = moment(this.horaFinal, formatoData)
      .add(offset + 0.016666667, 'minutes')
      .format(formatoData);
  }

  /**
   * preenche a propridade modulos e dadosTabelaParagem com os modulos não repetidos
   * @param data array com o registo raw vindo do web service
   */
  extrairModulos(data: RegistoLinha[]) {
    data.forEach(d => {
      const idModulo = d.idModulo;
      // inserir o modulo no array modulos se nao existir
      if (!this.modulos.find(m => m.idModulo === idModulo)) {
        const novoModulo: Modulo = {
          idModulo: idModulo,
          periodosOperacao: [],
          peakValues: [0, 0, 0, 0, 0]
        };
        this.modulos.push(novoModulo);
        this.xaxis.push('Módulo ' + idModulo);
        this.xaxisP.push('Módulo ' + idModulo);
        this.colunasTabelaParagem.push('M' + idModulo);
      }

      // inserir modulo no array de tabela de paragens se nao existir
      if (!this.dadosTabelaParagem.find(dt => dt.idModulo === idModulo)) {
        const novoDadoTabela: DadoTabelaParagem = {
          idModulo: idModulo,
          totalParagem: 0,
          totalOperacao: 0,
          ultimaParagem: '',
          ultimaRetoma: ''
        };
        this.dadosTabelaParagem.push(novoDadoTabela);
      }
    });
  }

  /**
   * Definir as labels do eixo dos XX do gráfico de produção
   * retorna array de string com as labels
   */
  definirXaxis(): string[] {
    const labels = [];
    this.modulos.forEach(m => labels.push('Módulo ' + m.idModulo));
    return labels;
  }

  /**
   * calcula o tempo de operação de cada módulo
   * guarda o tempo total de operação, o tempo total de paragem,
   * a data e a hora da última paragem e da última retoma
   * @param data array com o registo raw vindo do web service
   */
  calculaTempoOpModulo(data: RegistoLinha[]) {
    // nao há dados
    if (data.length === 0) {
      this.flagSemDados = true;
      // atualizar paragem para todos os modulos
      this.dadosTabelaParagem.forEach(dTp => {
        dTp.totalParagem += offset * 60; // incremententar total paragem em minutos
        const modulo = this.modulos.find(m => m.idModulo === dTp.idModulo);
        dTp.ultimaParagem =
          modulo.periodosOperacao[modulo.periodosOperacao.length - 1]; // atualizar a ultima paragem
      });
    } else {
      // Há dados mas falta algum modulo
      const modulosEmFalta = this.modulosEmFalta(data);
      modulosEmFalta.forEach(mEf => {
        const dadoTabela = this.dadosTabelaParagem.find(
          dTP => dTP.idModulo === mEf.idModulo
        );
        dadoTabela.totalParagem += offset * 60; // incrementa tempo paragem no modulo em falta
        dadoTabela.ultimaParagem =
          mEf.periodosOperacao[mEf.periodosOperacao.length - 1]; // atualizar a ultima paragem
      });
      // para os restates modulo
      data.forEach(d => {
        const modulo = this.modulos.find(m => m.idModulo === d.idModulo);
        const nRegistos = modulo.periodosOperacao.length;
        const dadoTabela = this.dadosTabelaParagem.find(
          dTp => dTp.idModulo === modulo.idModulo
        );
        if (nRegistos === 0) {
          // inserir o primeiro registo
          modulo.periodosOperacao.push(d.dataHora);
          dadoTabela.ultimaRetoma = d.dataHora;
        } else if (nRegistos % 2 === 0) {
          // existem um número par de registo
          // [inicio_ligação , fim_ligação]
          // insere novo registo e conta total operacao
          modulo.periodosOperacao.push(d.dataHora);
        } else {
          // se registo impar
          const horaRegisto = moment(
            modulo.periodosOperacao[nRegistos - 1],
            formatoData
          ); // utima hora registada
          const horaData = moment(d.dataHora, formatoData); // hora do registo
          const tempoDecorrido = horaData.diff(horaRegisto, 'seconds');
          if (tempoDecorrido <= 15) {
            // não houve paragem
            // substitui o ultimo registo de tempo. incrementar o tempo operacao
            modulo.periodosOperacao[nRegistos - 1] = d.dataHora;
            dadoTabela.totalOperacao += tempoDecorrido;
          } else {
            // se houve paragem
            // atualiza ultima retoma e ultma paragem.insere novo registo. incrementa tempo paragem
            dadoTabela.ultimaRetoma = d.dataHora;
            dadoTabela.ultimaParagem = modulo.periodosOperacao[nRegistos - 1];
            modulo.periodosOperacao.push(d.dataHora);
            if (this.flagSemDados) {
              dadoTabela.totalParagem = tempoDecorrido;
            } else {
              dadoTabela.totalParagem += tempoDecorrido;
            }
          }
        }
      });
      this.flagSemDados = false;
    }
    const tempos = [{ tempo: ['operação'] }, { tempo: ['paragem'] }];
    this.dadosTabelaParagem.forEach(d => {
      tempos[0].tempo.push(d.totalOperacao + '');
      tempos[1].tempo.push(d.totalParagem + '');
    });
    // this.dataSource = new MatTableDataSource(this.dadosTabelaParagem);
    this.dataSource = new MatTableDataSource(tempos);
  }

  novoChartSeriesParagem() {
    const operacao = [];
    const paragem = [];
    this.dadosTabelaParagem.forEach(dTp => {
      operacao.push(Math.round(dTp.totalOperacao / 60)); // em minutos
      paragem.push(Math.round(dTp.totalParagem / 60)); // em minutos
    });
    return [
      {
        name: 'operação',
        data: operacao
      },
      {
        name: 'paragem',
        data: paragem
      }
    ];
  }
  /**
   * verifica se data contem todos os módulos do array de modulos
   * @param data é json proveniente do servidor
   */
  modulosEmFalta(data: RegistoLinha[]): Modulo[] {
    const falta = new Array<Modulo>();
    this.modulos.forEach(m => {
      const existe = data.filter(d => d.idModulo === m.idModulo);
      if (existe.length === 0) {
        falta.push(m);
      }
    });
    return falta;
  }

  calcularResultadosPorModulo(data) {
    // reset o this.produtosPorModulo
    this.produtosPorModulo = [];

    // extrair todos os registos de cada módulo
    this.modulos.forEach(m => {
      const registosPorModulo = data.filter(function(registo) {
        return registo.idModulo === m.idModulo;
      });

      // calcular os aceites e os rejeitados
      const totalACeita = this.calcularACeita(registosPorModulo);
      const totalRejeita1 = this.calcularRejeita1(registosPorModulo);
      const totalRejeita2 = this.calcularRejeita2(registosPorModulo);

      const novoProdutoPorModulo = {
        modulo: m.idModulo,
        totalAceita: totalACeita,
        totalRejeitado1: totalRejeita1,
        totalRejeitado2: totalRejeita2
      };
      this.produtosPorModulo.push(novoProdutoPorModulo);
    });
  }

  calcularACeita(registosPorModulo) {
    let total = 0;
    for (const r of registosPorModulo) {
      if (r.aceita === '1') {
        total++;
      }
    }
    return total;
  }

  calcularRejeita1(registosPorModulo) {
    let total = 0;
    for (const r of registosPorModulo) {
      if (r.rejeita1 === '1') {
        total++;
      }
    }
    return total;
  }
  calcularRejeita2(registosPorModulo) {
    let total = 0;
    for (const r of registosPorModulo) {
      if (r.rejeita2 === '1') {
        total++;
      }
    }
    return total;
  }

  calcularTotais(data) {
    // incrementar os totais
    data.forEach(r => {
      if (r.aceita === '1') {
        this.totalAceite++;
      } else if (r.rejeita1 === '1') {
        this.totalRejeitado1++;
      } else if (r.rejeita2 === '1') {
        this.totalRejeitado2++;
      }
    });
    this.totalProcessado += data.length;

    this.dadosTabela = [
      {
        produto: 'Aceita',
        total: this.totalAceite,
        percentagem: (this.totalAceite / this.totalProcessado) * 100
      },
      {
        produto: 'Rejeita 1',
        total: this.totalRejeitado1,
        percentagem: (this.totalRejeitado1 / this.totalProcessado) * 100
      },
      {
        produto: 'Rejeita 2',
        total: this.totalRejeitado2,
        percentagem: (this.totalRejeitado2 / this.totalProcessado) * 100
      }
    ];
  }

  novoChartSeries() {
    const aceitesPorModulo = [];
    const rejeitados1PorModulo = [];
    const rejeitados2PorModulo = [];

    this.produtosPorModulo.forEach(p => {
      aceitesPorModulo.push(p.totalAceita);
      rejeitados1PorModulo.push(p.totalRejeitado1);
      rejeitados2PorModulo.push(p.totalRejeitado2);
    });

    const novaSerie = [
      {
        name: 'Ac',
        data: aceitesPorModulo
      },
      {
        name: 'R1',
        data: rejeitados1PorModulo
      },
      {
        name: 'R2',
        data: rejeitados2PorModulo
      }
    ];
    return novaSerie;
  }

  peakValue(data: RegistoLinha[]) {
    this.optionsPeakValue = [];
    this.modulos.forEach(m => {
      const registoModulo = data.filter(d => d.idModulo === m.idModulo);
      registoModulo.forEach(rM => {
        const valor = rM.peakValue;

        if (valor >= 0 && valor < 1) {
          m.peakValues[0]++;
        } else if (valor >= 1 && valor < 2) {
          m.peakValues[1]++;
        } else if (valor >= 2 && valor < 3) {
          m.peakValues[2]++;
        } else if (valor >= 3 && valor < 4) {
          m.peakValues[3]++;
        } else {
          m.peakValues[4]++;
        }
      });
      this.definirParamGraficoPeakValue({
        name: m.idModulo,
        data: m.peakValues
      });
    });
  }

  definirParametros() {
    // parametros para gráfico de produção
    this.optionsProducao = {
      chart: {
        height: screen.width > 1400 && screen.height > 800 ? 400 : 250,
        // height: 250,
        // height: 450,
        type: 'bar',
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '100%',
          endingShape: 'rounded'
        },
        dataLabels: {
          position: 'top'
        }
      },
      dataLabels: {
        enabled: true,
        formatter: function(val, { seriesIndex, dataPointIndex, w }) {
          // return w.config.series[seriesIndex].name + ' = ' + val;
          return val;
        },
        offsetY: -21,
        style: {
          fontSize: '25px',
          fontFamily: 'Helvetica, Arial, sans-serif',
          colors: ['#e2e1e1', '#e2e1e1', '#e2e1e1']
        }
      },
      stroke: {
        show: true,
        width: 1,
        colors: ['transparent']
      },
      series: this.chartSerie,
      xaxis: {
        categories: this.xaxis,
        labels: {
          style: {
            fontSize: '1rem',
            fontFamily: 'Helvetica, Arial, sans-serif'
          }
        }
      },
      colors: ['#0b6252', '#838685', '#2bc5a9'],
      yaxis: {
        show: true,
        maxWidth: 27,
        labels: {
          style: {
            fontSize: '1rem',
            fontFamily: 'Helvetica, Arial, sans-serif'
          }
        }
      },
      legend: {
        show: true,
        fontSize: '15rem',
        position: 'top',
        horizontalAlign: 'left',
        offsetX: 100,
        markers: {
          width: 10,
          height: 10
        }
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        y: {
          formatter: function(val) {
            return val;
          }
        }
      },
      noData: {
        text: 'SEM DADOS',
        align: 'center',
        verticalAlign: 'middle',
        offsetX: 5,
        offsetY: 5,
        style: {
          color: 'red',
          fontSize: '60px',
          fontFamily: 'arial'
        }
      }
    };
  }

  definirParametrosGraficoParagem() {
    // parametros para gráfico das paragens
    this.optionsParagem = {
      chart: {
        height: screen.width > 1400 && screen.height > 800 ? 400 : 250,
        // height: 250,
        // height: 450,
        type: 'bar',
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        bar: {
          horizontal: false,
          columnWidth: '100%',
          endingShape: 'rounded'
        },
        dataLabels: {
          position: 'top'
        }
      },
      dataLabels: {
        enabled: true,
        formatter: function(val, { seriesIndex, dataPointIndex, w }) {
          // return w.config.series[seriesIndex].name + ' = ' + val;
          return val;
        },
        offsetY: -21,
        style: {
          fontSize: '25px',
          fontFamily: 'Helvetica, Arial, sans-serif',
          colors: ['#e2e1e1', '#e2e1e1', '#e2e1e1']
        }
      },
      stroke: {
        show: true,
        width: 1,
        colors: ['transparent']
      },
      series: this.chartSerieP,
      xaxis: {
        categories: this.xaxisP,
        labels: {
          style: {
            fontSize: '1rem',
            fontFamily: 'Helvetica, Arial, sans-serif'
          }
        }
      },
      colors: ['#3bcea7', '#c37d7d'],
      yaxis: {
        show: true,
        labels: {
          maxWidth: 27,
          style: {
            fontSize: '1rem',
            fontFamily: 'Helvetica, Arial, sans-serif'
          }
        }
      },
      legend: {
        show: true,
        fontSize: '15rem',
        position: 'top',
        horizontalAlign: 'left',
        offsetX: 100,
        markers: {
          width: 11,
          height: 10
        }
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        y: {
          formatter: function(val) {
            return val;
          }
        }
      },
      noData: {
        text: 'SEM DADOS',
        align: 'center',
        verticalAlign: 'middle',
        offsetX: 5,
        offsetY: 5,
        style: {
          color: 'red',
          fontSize: '60px',
          fontFamily: 'arial'
        }
      }
    };
  }

  definirParamGraficoPeakValue(serie?: any) {
    // parâmetros para o gráfico do peak value
    this.optionsPeakValue.push({
      chart: {
        height: screen.width > 1400 && screen.height > 800 ? 330 : 160,
        // height: 160,
        // height: 400,
        width: screen.width > 1400 ? 300 : 220,
        // width: 220,
        type: 'bar',
        toolbar: {
          show: false
        }
      },
      plotOptions: {
        containerMargin: {
          left: 0,
          top: 4,
          right: 0,
          bottom: 0
        },
        bar: {
          horizontal: false,
          columnWidth: '90%',
          endingShape: 'rounded'
        },
        dataLabels: {
          position: 'top',
          maxItems: 100,
          hideOverflowingLabels: true
        }
      },
      dataLabels: {
        enabled: false,
        enabledOnSeries: undefined,
        formatter: function(val, { seriesIndex, dataPointIndex, w }) {
          return w.config.series[seriesIndex].name + ' = ' + val;
        },
        textAnchor: 'middle',
        offsetX: 0,
        offsetY: 0,
        style: {
          fontSize: '25px',
          fontFamily: 'Helvetica, Arial, sans-serif',
          colors: ['#7abef3', '#a1e2b9', '#f5c980']
        },
        dropShadow: {
          enabled: false,
          top: 1,
          left: 1,
          blur: 1,
          opacity: 0
        }
      },
      stroke: {
        show: false,
        width: 10,
        colors: ['transparent']
      },
      series: [serie],
      xaxis: {
        categories: this.xaxisDN,
        labels: {
          style: {
            fontSize: '0.8rem',
            fontFamily: 'Helvetica, Arial, sans-serif'
          }
        }
      },
      colors: ['rgb(118, 169, 155)'],
      yaxis: {
        show: true,
        labels: {
          maxWidth: 27,
          style: {
            fontSize: '0.8rem',
            fontFamily: 'Helvetica, Arial, sans-serif'
          },
          offsetX: 0,
          offsetY: 0
        }
      },
      fill: {
        opacity: 1
      },
      tooltip: {
        y: {
          formatter: function(val) {
            return val;
          }
        }
      },
      noData: {
        text: 'SEM DADOS',
        align: 'center',
        verticalAlign: 'middle',
        offsetX: 5,
        offsetY: 5,
        style: {
          color: 'red',
          fontSize: '60px',
          fontFamily: 'arial'
        }
      },
      grid: {
        show: true,
        yaxis: {
          lines: {
            show: false
          }
        },
        padding: {
          top: 0,
          right: 0,
          bottom: 0,
          left: 0
        }
      }
    });
  }
}
