import { RegistoLinha } from "./../_interfaces/registo-linha.interface";
import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root",
})
export class GetRegistosLinhaService {
  constructor(private http: HttpClient) {}

  getRegistosLinha(
    idLinha: string,
    dataHoraInic?: string,
    dataHoraFim?: string
  ) {
    const body = {
      token: "wJLUdu43wyqdLpFtjL",
      idLinha: idLinha,
      dataHoraInic: dataHoraInic,
      dataHoraFim: dataHoraFim,
    };
    return this.http.post<{
      // o resultado a ser devolvido
      response: string;
      payload: {
        result?: RegistoLinha[];
      };
    }>("https://api.url/registos-linha", JSON.stringify(body));
  }
}
