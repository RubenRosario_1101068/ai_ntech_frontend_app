import { Component, OnInit, Input } from '@angular/core';
// @ts-ignore
import ApexCharts from 'apexcharts';

@Component({
  selector: 'app-apexchart',
  templateUrl: './apex-chart.component.html',
  styleUrls: ['./apex-chart.component.css']
})
export class ApexChartComponent implements OnInit {
  @Input()
  name: string;

  @Input()
  options: any;

  chart: any;

  constructor() {}

  ngOnInit() {
    this.chart = new ApexCharts(
      document.getElementById(this.name),
      this.options
    );
    this.chart.render();
  }

  updateDataSeries(data: any) {
    this.chart.updateSeries(data, true);
  }

  updateOptions(options: any, redraw: boolean, animate: boolean) {
    this.chart.updateOptions(options, redraw, animate);
  }
}
