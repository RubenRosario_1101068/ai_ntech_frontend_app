export interface Producao {
  classificacao: string;
  quantidade: number;
  percentagem: number;
}
