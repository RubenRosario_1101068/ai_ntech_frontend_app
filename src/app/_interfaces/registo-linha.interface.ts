export interface RegistoLinha {
  idLinha: string;
  idModulo: string;
  idChamber: string;
  dataHora: string;
  lote: string;
  peakValue: number;
  aceita: number;
  rejeita1: number;
  rejeita2: number;
  Shift: string;
}
